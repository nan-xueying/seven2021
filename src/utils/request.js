// 写我们的请求工具类  
import axios from 'axios'
const server = axios.create({
    baseURL:'http://localhost:8081',
    timeout:5000
})
server.interceptors.request.use(config=>{
    return config
})

server.interceptors.response.use(res=>{
    return res.data
})
export default server