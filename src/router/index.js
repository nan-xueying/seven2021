import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../layout/Layout'
import Home from '../views/home/Home'
import Control from '../views/kongzhi/Control'
import History from '../views/lishi/History'
import Detail from '../views/xiangqing/Detail'
import Threshold from '../views/yuzhi/Threshold'
Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    redirect:'/home'
  },
  {
    path:'/',
    name:'Layout',
    meta:{title:'布局'},
    component:Layout,
    children:[
      {
        path:'/home',
        name:'Home',
        component:Home,
        meta:{title:'首页'}
      },
      {
        path:'/control',
        name:'Control',
        component:Control,
        meta:{title:'控制页'}
      },
      {
        path:'/history',
        name:'History',
        component:History,
        meta:{title:'历史数据页'}
      },
      {
        path:'/detail',
        name:'Detail',
        component:Detail,
        meta:{title:'详细页'}
      },
      {
        path:'/threshold',
        name:'Threshold',
        component:Threshold,
        meta:{title:'阈值控制页'}
      },
    ]
  }

]

const router = new VueRouter({
  routes
})

export default router
